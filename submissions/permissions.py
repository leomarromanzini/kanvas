from rest_framework.permissions import BasePermission


class FacilitadorPermission(BasePermission):

    message = {"detail": "You do not have permission to perform this action."}

    def has_permission(self, request, view):

        return request.user.is_staff
