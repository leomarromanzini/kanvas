from activities.models import Activity
from courses.exceptions import InvalidUserType
from django.db import IntegrityError
from rest_framework import permissions, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from submissions.models import Submission
from submissions.serializers import (PostSubmissionInActivitySerializer,
                                     SubmissionSerializer,
                                     UpdateGradeSerializer)

from .permissions import FacilitadorPermission


class SubmissionView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, activity_id=""):

        try:
            request.data["user_id"] = request.user.id

            if request.user.is_staff or request.user.is_superuser:
                raise InvalidUserType

            if request.data.get("grade"):
                del request.data["grade"]

            serializer = PostSubmissionInActivitySerializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            activity = Activity.objects.get(pk=activity_id)

            submission = Submission.objects.create(**request.data, activity=activity)

            serialized = SubmissionSerializer(submission)

            return Response(serialized.data, status=status.HTTP_201_CREATED)
        except Activity.DoesNotExist:
            return Response(
                {"error": "invalid activity_id"}, status=status.HTTP_404_NOT_FOUND
            )
        except InvalidUserType:
            return Response(
                {"error": "Only students can submit"}, status=status.HTTP_403_FORBIDDEN
            )


class SubmissionAuthView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [FacilitadorPermission]

    def put(self, request, submission_id=""):
        try:
            submission = Submission.objects.get(pk=submission_id)
            serializer = UpdateGradeSerializer(submission, data=request.data)
            serializer.is_valid(raise_exception=True)

            submission.grade = request.data["grade"]
            submission.save()

            serialized = SubmissionSerializer(submission)

            return Response(serialized.data)

        except Submission.DoesNotExist:
            return Response(
                {"errors": "invalid submission_id"}, status=status.HTTP_404_NOT_FOUND
            )


class GetSubmissionView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):

        if request.user.is_staff:

            submissions = Submission.objects.all()

            serialized = SubmissionSerializer(submissions, many=True)

            return Response(serialized.data)

        else:
            submissions = Submission.objects.filter(user_id=request.user.id)

            serialized = SubmissionSerializer(submissions, many=True)

            return Response(serialized.data)
