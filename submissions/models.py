from django.db import models
from activities.models import Activity


class Submission(models.Model):
    grade = models.IntegerField(null=True)
    repo = models.CharField(max_length=255)
    user_id = models.IntegerField()
    activity = models.ForeignKey(
        "activities.Activity", on_delete=models.CASCADE, related_name="submissions"
    )
