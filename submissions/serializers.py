from rest_framework import serializers


class SubmissionSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    grade = serializers.IntegerField(read_only=True)
    repo = serializers.CharField()
    user_id = serializers.IntegerField(read_only=True)
    activity_id = serializers.IntegerField()


class PostSubmissionInActivitySerializer(serializers.Serializer):
    grade = serializers.IntegerField(read_only=True)
    repo = serializers.CharField()


class UpdateGradeSerializer(serializers.Serializer):
    grade = serializers.IntegerField()
