from django.urls import path
from submissions.views import GetSubmissionView, SubmissionView, SubmissionAuthView

urlpatterns = [
    path("activities/<int:activity_id>/submissions/", SubmissionView.as_view()),
    path("submissions/<int:submission_id>/", SubmissionAuthView.as_view()),
    path("submissions/", GetSubmissionView.as_view()),
]
