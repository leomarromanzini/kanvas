from django.urls import path

from activities.views import (
    ActivityByIdView,
    ActivityView,
)

urlpatterns = [
    path("activities/", ActivityView.as_view()),
    path("activities/<int:activity_id>/", ActivityByIdView.as_view()),
]
