from django.db import IntegrityError
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView
from submissions.models import Submission

from activities.exceptions import ActivityNotEmpty
from activities.models import Activity
from activities.serializers import ActivitySerializer

from .permissions import FacilitadorPermission


class ActivityView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [FacilitadorPermission]

    def post(self, request):
        try:
            serializer = ActivitySerializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            new_activity = Activity.objects.create(**request.data)
            serialized = ActivitySerializer(new_activity)

            return Response(serialized.data, status=status.HTTP_201_CREATED)

        except IntegrityError:
            return Response(
                {"error": "Activity with this name already exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    def get(self, _):

        activities = Activity.objects.all()
        serialized = ActivitySerializer(activities, many=True)

        return Response(serialized.data)


class ActivityByIdView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [FacilitadorPermission]

    def put(self, request, activity_id=""):

        try:
            activity = Activity.objects.get(pk=activity_id)
            submissions = Submission.objects.filter(activity_id=activity_id)

            if submissions:
                raise ActivityNotEmpty

            serializer = ActivitySerializer(activity, data=request.data)
            serializer.is_valid(raise_exception=True)

            activity.title = request.data["title"]
            activity.points = request.data["points"]
            activity.save()

            return Response(serializer.data)

        except IntegrityError:
            return Response(
                {"error": "Activity with this name already exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except Activity.DoesNotExist:
            return Response(
                {"error": "invalid activity_id"}, status=status.HTTP_404_NOT_FOUND
            )
        except ActivityNotEmpty:
            return Response(
                {"error": "You can not change an Activity with submissions"},
                status=status.HTTP_400_BAD_REQUEST,
            )
