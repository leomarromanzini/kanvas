from django.urls import path
from .views import AccountCreateView, login

urlpatterns = [path("accounts/", AccountCreateView.as_view()), path("login/", login)]
