from accounts.models import User
from django.http.response import HttpResponseBadRequest
from rest_framework.views import APIView
from accounts.serializers import CreateUserSerializer
from rest_framework.response import Response
from rest_framework import status
from django.db import IntegrityError
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)


class AccountCreateView(APIView):
    def post(self, request):

        try:
            if request.data["is_superuser"]:
                newUser = User.objects.create_superuser(
                    username=request.data["username"],
                    password=request.data["password"],
                    is_staff=request.data["is_staff"],
                )

            else:
                newUser = User.objects.create_user(
                    username=request.data["username"],
                    password=request.data["password"],
                    is_staff=request.data["is_staff"],
                )

            serialized = CreateUserSerializer(newUser)
            print(serialized)
            return Response(serialized.data, status=status.HTTP_201_CREATED)

        except IntegrityError:
            return Response(
                {"detail: User already registered"}, status=status.HTTP_409_CONFLICT
            )


@api_view(["POST"])
def login(request):
    try:
        user = authenticate(
            username=request.data["username"],
            password=request.data["password"],
        )

        if user:
            token = Token.objects.get_or_create(user=user)[0]
            return Response({"token": token.key})

        return Response(
            {"detail": "Invalid credentials"}, status=status.HTTP_401_UNAUTHORIZED
        )

    except KeyError:
        return Response(
            {"detail": "Invalid fields"}, status=status.HTTP_400_BAD_REQUEST
        )
