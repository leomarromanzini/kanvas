from django.urls import path

from courses.views import (
    CourseView,
    CourseRegisterView,
    GetCourseByIdView,
)

urlpatterns = [
    path("courses/", CourseView.as_view()),
    path("courses/<int:course_id>/", GetCourseByIdView.as_view()),
    path("courses/<int:course_id>/registrations/", CourseRegisterView.as_view()),
]
