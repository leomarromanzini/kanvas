from rest_framework.permissions import BasePermission


class InstructorPermission(BasePermission):

    message = {"detail": "You do not have permission to perform this action."}

    def has_permission(self, request, view):

        if request.method == "GET":
            return True

        return request.user.is_superuser


class FacilitadorPermission(BasePermission):

    message = {"detail": "You do not have permission to perform this action."}

    def has_permission(self, request, view):

        return request.user.is_staff
