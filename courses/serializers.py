from rest_framework import serializers

from accounts.serializers import UserSerializer
from courses.models import Course


class CourseSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    users = UserSerializer(many=True, read_only=True)

    # def create(self, validated_data):

    #     return Course.objects.create(**validated_data)

    # def update(self, instance, validated_data):

    #     instance.name = validated_data.get("name", instance.name)
    #     instance.save()

    #     return instance


class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    username = serializers.CharField()


class UsersInCourseSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    users = UserSerializer(many=True)
