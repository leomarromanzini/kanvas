from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from courses.models import Course
from courses.serializers import (
    CourseSerializer,
    UsersInCourseSerializer,
)
from django.db import IntegrityError
from accounts.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from .permissions import InstructorPermission
from .exceptions import InvalidUserType, InvalidTypeOfData


class CourseView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [InstructorPermission]

    def post(self, request):
        try:
            serializer = CourseSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            new_course = Course.objects.create(**request.data)
            serialized = CourseSerializer(new_course)

            return Response(serialized.data, status=status.HTTP_201_CREATED)
        except IntegrityError:
            return Response(
                {"error": "Course with this name already exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    def get(self, _):

        courses = Course.objects.all()

        serialized = UsersInCourseSerializer(courses, many=True)

        return Response(serialized.data)


class GetCourseByIdView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [InstructorPermission]

    def get(self, _, course_id):

        try:
            course = Course.objects.get(pk=course_id)
            serialized = UsersInCourseSerializer(course)

            return Response(serialized.data)

        except Course.DoesNotExist:
            return Response(
                {"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND
            )

    def delete(self, _, course_id):

        try:
            course = Course.objects.get(pk=course_id)
            course.delete()

            return Response(status=status.HTTP_204_NO_CONTENT)

        except Course.DoesNotExist:
            return Response(
                {"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND
            )

    def put(self, request, course_id):

        course = Course.objects.get(pk=course_id)

        try:
            serializer = CourseSerializer(course, data=request.data)
            serializer.is_valid(raise_exception=True)

            # serializer.save()
            course.name = request.data["name"]
            course.save()

            return Response(serializer.data)

        except IntegrityError:
            return Response(
                {"error": "Course with this name already exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except Course.DoesNotExist:
            return Response(
                {"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND
            )


class CourseRegisterView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, InstructorPermission]

    def put(self, request, course_id=""):

        try:
            users = request.data["user_ids"]
            course = Course.objects.get(pk=course_id)

            if not isinstance(users, list):
                raise InvalidTypeOfData

            course.users.clear()

            for user_id in request.data["user_ids"]:
                user = User.objects.get(pk=user_id)

                if user.is_staff or user.is_superuser:
                    raise InvalidUserType

                course.users.add(user)

            serialized = UsersInCourseSerializer(course)

            return Response(serialized.data)
        except InvalidUserType:
            return Response(
                {"errors": "Only students can be enrolled in the course."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except Course.DoesNotExist:
            return Response(
                {"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND
            )
        except User.DoesNotExist:
            return Response(
                {"errors": "invalid user_id"}, status=status.HTTP_404_NOT_FOUND
            )
        except InvalidTypeOfData:
            return Response(
                {"errors": "invalid user_id list"}, status=status.HTTP_400_BAD_REQUEST
            )
        # except KeyError:
        #     return Response(
        #         {"errors": "Caiu no keyerror"}, status=status.HTTP_400_BAD_REQUEST
        #     )
