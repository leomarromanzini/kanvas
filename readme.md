# Como instalar e rodar? 🚀

Para instalar o sistema, é necessário seguir alguns passos, como baixar o projeto e fazer instalação das dependências. Para isso, é necessário abrir uma aba do terminal e digitar o seguinte:

## Este passo é para baixar o projeto

git clone https://gitlab.com/leomarromanzini/kanvas.git

Depois que terminar de baixar, é necessário entrar na pasta, criar um ambiente virtual e entrar nele:

- Entrar na pasta

      cd kanvas

- Criar um ambiente virtual

      python3 -m venv venv

- Entrar no ambiente virtual

      source venv/bin/activate

## Então, para instalar as dependências, basta:

    pip install -r requirements.txt

Depois de ter instalado as dependências, é necessário rodar as migrations para que o banco de dados e as tabelas sejam criadas:

    ./manage.py migrate

Então, para rodar, basta digitar o seguinte, no terminal:

    ./manage.py runserver

E o sistema estará rodando em http://127.0.0.1:8000/

# Utilização 🖥️

Para utilizar este sistema, é necessário utilizar um API Client, como o Insomnia

## Sobre proteção das rotas

Algumas rotas são protegidas por autenticação de token, para utilizar essas rotas é necessário passar no header da requisição:

| header        | value                                         |
| ------------- | --------------------------------------------- |
| Authorization | Token d14797d75b8233as234da6f1502123114435918 |

A proteção de rotas também contém níveis de permissão, para criar a permissão de um usuário use a tabela abaixo como exemplo:
| |Instrutor|Facilitador|Estudante|
|------------|---------|-----------|---------|
|is_superuser|True |False |False |
|is_staff |True |True |False |

> As rotas protegidas contarão com um aviso sobre o tipo de permissões dela

# Rotas

## User

### `POST /api/accounts/`

Cria um novo usuário.

Body:

```json
{
  "username": "student",
  "password": "1234",
  "is_superuser": false,
  "is_staff": false
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 201 (created)
{
  "id": 1,
  "username": "student",
  "is_superuser": false,
  "is_staff": false
}
```

### `POST /api/login/`

Faz login de um usuário.

Body:

```json
{
  "username": "student",
  "password": "1234"
}
```

Response:

```json
//  RESPONSE STATUS -> HTTP 200 (OK)
{
  "token": "d14797d75b8233as234da6f1502123114435918"
}
```

## Courses

### `GET /api/courses/`

Retorna todos dos cursos cadastrados.

Body:

```json

```

Response:

```json
// RESPONSE STATUS -> HTTP 20O (OK)
[
  {
    "id": 1,
    "name": "Django",
    "users": []
  },
  {
    "id": 2,
    "name": "Node.js",
    "users": [
      {
        "id": 3,
        "username": "leo3"
      }
    ]
  },
  {
    "id": 3,
    "name": "Nestjs",
    "users": [
      {
        "id": 1,
        "username": "leo"
      }
    ]
  }
]
```

### `GET /api/courses/<int:course_id>/`

Retorna um curso pelo id.

Body:

```json

```

Response:

```json
// RESPONSE STATUS -> HTTP 20O (OK)
{
  "id": 2,
  "name": "Node.js",
  "users": [
    {
      "id": 3,
      "username": "leo3"
    }
  ]
}
```

### `POST /api/courses/`

> **Protegida - Instrutor**

Cria um novo curso

Body:

```json
// Header -> Authorization: Token <token-do-instrutor>
{
  "name": "Lógica de programação"
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 201 (CREATED)
{
  "id": 1,
  "name": "Lógica de programação",
  "users": []
}
```

### `PUT /api/courses/<int:course_id>/`

> **Protegida - Instrutor**

Altera o nome do curso pelo id

Body:

```json
//RESPONSE STATUS -> HTTP 200 (OK)
{
  "name": "Lógica de programação!"
}
```

Response:

```json
{
  // RESPONSE STATUS -> HTTP 200 (OK)
  "id": 1,
  "name": "Lógica de programação!",
  "users": []
}
```

### `PUT /api/courses/<int:course_id>/registrations/`

> **Protegida - Instrutor**

Coloca estudantes no curso

> Apenas usuários de nível estudantes poder ser adicionados

Body:

```json
// Header -> Authorization: Token <token-do-instrutor>
{
  "user_ids": [4]
}
```

Response:

```json
{
// RESPONSE STATUS -> HTTP 200
{

  "id": 3,
  "name": "Lógica de programação!",
  "users": [
    {
      "id": 4,
      "username": "user4"
    }
  ]
}
}
```

### `DELETE /api/courses/<int:course_id>/`

> **Protegida - Instrutor**

Deleta um curso pelo id

Body:

```json
// REQUEST
// Header -> Authorization: Token <token-do-instrutor>

```

Response:

```json

// RESPONSE STATUS -> HTTP 204 NO CONTENT
```

## Activities

### `POST /api/activities/`

Cria uma nova atividade

> **Protegida - Instrutor, facilitador**

Body:

```json
// REQUEST
// Header -> Authorization: Token <token-do-facilitador ou token-do-instrutor>
{
  "title": "Kenzie Pet",
  "points": 10
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 201
{
  "id": 1,
  "title": "Kenzie Pet",
  "points": 10,
  "submissions": []
}
```

### `GET /api/activities/`

Lista todas as atividades cadastradas

> **Protegida - Instrutor, facilitador**

Body:

```json
// REQUEST
// Header -> Authorization: Token <token-do-instrutor ou token-do-facilitador>

```

Response:

```json
// RESPONSE STATUS -> HTTP 200
[
  {
    "id": 1,
    "title": "Kenzie Pet",
    "points": 10,
    "submissions": []
  },
  {
    "id": 2,
    "title": "Kanvas",
    "points": 10,
    "submissions": [
      {
        "id": 2,
        "grade": 8,
        "repo": "http://gitlab.com/kanvas",
        "user_id": 4,
        "activity_id": 2
      }
    ]
  }
]
```

### `PUT /api/activities/<int:activity_id>/`

Atualiza uma atividade

> **Protegida - Instrutor, facilitador**
> Caso a atividade tenha submissões não é possível alterar ela

Body:

```json
// REQUEST
// Header -> Authorization: Token <token-do-instrutor ou token-do-facilitador>
{
  "title": "Kenzie Pet11",
  "points": 2
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 200
{
  "id": 4,
  "title": "Kenzie Pet11",
  "points": 2.0,
  "submissions": []
}
```

## Submissions

### `POST /api/activities/<int:activity_id>/submissions/`

Adiciona uma submissão à atividade

> **Protegida - Estudante**
> Apenas estudantes podem submeter envios

Body:

```json
// REQUEST
// Header -> Authorization: Token <token-do-estudante>
{
  "grade": 10, // Esse campo é opcional
  "repo": "http://gitlab.com/kenzie_pet"
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 201
{
  "id": 7,
  "grade": null,
  "repo": "http://gitlab.com/kenzie_pet",
  "user_id": 3,
  "activity_id": 1
}
```

### `PUT /api/submissions/<int:submission_id>/`

Edita a nota de uma submissão

> **Protegida - Instrutor, facilitador**

Body:

```json
// REQUEST
// Header -> Authorization: Token <token-do-facilitador ou token-do-instrutor>
{
  "grade": 10
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 200
{
  "id": 3,
  "grade": 10,
  "repo": "http://gitlab.com/kenzie_pet",
  "user_id": 3,
  "activity_id": 1
}
```

### `GET /api/submissions/`

Lista todas as submissões

> **Protegida - Instrutor, facilitador e estudante**
> Caso um estudante faça a requisição apenas as submissões do estudante serão retornadas
> Caso um instrutor ou facilitador faça a requisição todas submissões serão retornadas

Body:

```json
//REQUEST
//Header -> Authorization: Token <token-do-estudante>

```

Response :

```json
// RESPONSE STATUS -> HTTP 200
[
  {
    "id": 2,
    "grade": 8,
    "repo": "http://gitlab.com/kanvas",
    "user_id": 4,
    "activity_id": 2
  },
  {
    "id": 5,
    "grade": null,
    "repo": "http://gitlab.com/kmdb2",
    "user_id": 4,
    "activity_id": 1
  }
]
```

## Tecnologias utilizadas 📱

- Django
- Django Rest Framework
- SQLite

---

## Licence

MIT
